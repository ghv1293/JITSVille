package com.jitsville;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import com.JITSVille.Journey;
import com.JITSVille.Passenger;
import com.JITSVille.PassengerBean;
import com.JITSVille.PassengerPattern;

public class PassengerTest {

	PassengerPattern passengerPattern;
	PassengerBean passengerBean;
	Passenger commuter;
	Passenger vacation;
	Journey journey;
	List<PassengerBean> passengerList;
	@Before
	public void BeforeTest() {
		
		passengerPattern = new PassengerPattern();
		passengerBean = new PassengerBean();
		commuter = passengerPattern.getPassenger("Commuter");
		vacation = passengerPattern.getPassenger("Vacation");
		journey = new Journey();
	}
	
	@Test
	public void testCommuters() {
		
		passengerBean.setFrequentRider(false);
		passengerBean.setStops(10);
		double actual= commuter.caclFare(passengerBean);
		double expected = 5;
		assertEquals(expected, actual, .001);
	}
	
	@Rule
	public ExpectedException expectedException =  ExpectedException.none();
	
	@Test
	public void testVactionException() {

		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("More Miles");
		passengerBean.setMiles(4);
		vacation.caclFare(passengerBean);
	}
	
	@Test
	public void testVaction() {
		
		passengerBean.setMiles(90);
		double actual= vacation.caclFare(passengerBean);
		double expected = 45;
		assertEquals(expected, actual, .001);
	}
	
	@Test
	public void testMeals() {
		
		passengerBean.setMiles(5);
		vacation.caclFare(passengerBean);
		int actual= passengerBean.getCalculatedMeals();
		int expected = 1;
		assertEquals(expected, actual, .001);
	}
	@Test
	public void testMeals2() {
		
		passengerBean.setMiles(100);
		vacation.caclFare(passengerBean);
		int actual= passengerBean.getCalculatedMeals();
		int expected = 1;
		assertEquals(expected, actual, .001);
	}
	@Test
	public void testMeals3() {
		
		passengerBean.setMiles(185);
		vacation.caclFare(passengerBean);
		int actual= passengerBean.getCalculatedMeals();
		int expected = 2;
		assertEquals(expected, actual, .001);
	}
	
	@Test
	public void testTotalNewspaper() {

		int expected = 3;
		
		
		
		List<PassengerBean> passengerList  =new ArrayList<PassengerBean>();
		
		PassengerBean passengerBeanLocal1 = new PassengerBean();
		passengerBeanLocal1.setStops(3);
		passengerBeanLocal1.setFrequentRider(true);
		commuter.caclFare(passengerBeanLocal1);
		passengerList.add(passengerBeanLocal1);
		
		PassengerBean passengerBeanLocal2 = new PassengerBean();
		passengerBeanLocal2.setStops(5);
		passengerBeanLocal2.setFrequentRider(true);
		commuter.caclFare(passengerBeanLocal2);
		passengerList.add(passengerBeanLocal2);
		
		PassengerBean passengerBeanLocal3 = new PassengerBean();
		passengerBeanLocal3.setStops(4);
		passengerBeanLocal3.setFrequentRider(false);
		passengerBeanLocal3.setRequestedNewspaper(1);
		commuter.caclFare(passengerBeanLocal3);
		passengerList.add(passengerBeanLocal3);
		
		PassengerBean passengerBeanLocal4= new PassengerBean();
		passengerBeanLocal4.setMiles(90);
		passengerBeanLocal4.setRequestedNewspaper(1);
		vacation.caclFare(passengerBeanLocal4);
		passengerList.add(passengerBeanLocal4);
		
		PassengerBean passengerBeanLocal5 = new PassengerBean();
		passengerBeanLocal5.setMiles(199);
		passengerBeanLocal5.setRequestedNewspaper(1);
		vacation.caclFare(passengerBeanLocal5);
		passengerList.add(passengerBeanLocal5);
		
		int actual = journey.totalNewspaper(passengerList);
		
		
		assertEquals(expected, actual);
		
	}
	
	@Test
	public void testTotalMeals() {
		
		int expected = 3;

		List<PassengerBean> passengerList  =new ArrayList<PassengerBean>();
		
		PassengerBean passengerBeanLocal1 = new PassengerBean();
		passengerBeanLocal1.setStops(3);
		passengerBeanLocal1.setFrequentRider(true);
		commuter.caclFare(passengerBeanLocal1);
		passengerList.add(passengerBeanLocal1);
		
		PassengerBean passengerBeanLocal2 = new PassengerBean();
		passengerBeanLocal2.setStops(5);
		passengerBeanLocal2.setFrequentRider(true);
		commuter.caclFare(passengerBeanLocal2);
		passengerList.add(passengerBeanLocal2);
		
		PassengerBean passengerBeanLocal3 = new PassengerBean();
		passengerBeanLocal3.setStops(4);
		passengerBeanLocal3.setFrequentRider(false);
		passengerBeanLocal3.setRequestedNewspaper(1);
		commuter.caclFare(passengerBeanLocal3);
		passengerList.add(passengerBeanLocal3);
		
		PassengerBean passengerBeanLocal4= new PassengerBean();
		passengerBeanLocal4.setMiles(90);
		passengerBeanLocal4.setRequestedNewspaper(1);
		vacation.caclFare(passengerBeanLocal4);
		passengerList.add(passengerBeanLocal4);
		
		PassengerBean passengerBeanLocal5 = new PassengerBean();
		passengerBeanLocal5.setMiles(199);
		passengerBeanLocal5.setRequestedNewspaper(1);
		vacation.caclFare(passengerBeanLocal5);
		passengerList.add(passengerBeanLocal5);
		
		int actual = journey.totalMeals(passengerList);
		
		assertEquals(expected, actual);
	}
	
	@Test
	public void testTotalCost() {
		double expected = 5.6;
		List<PassengerBean> passengerList  =new ArrayList<PassengerBean>();
		
		PassengerBean passengerBeanLocal1 = new PassengerBean();
		passengerBeanLocal1.setStops(3);
		passengerBeanLocal1.setFrequentRider(true);
		commuter.caclFare(passengerBeanLocal1);
		passengerList.add(passengerBeanLocal1);
		
		PassengerBean passengerBeanLocal2 = new PassengerBean();
		passengerBeanLocal2.setStops(5);
		passengerBeanLocal2.setFrequentRider(true);
		commuter.caclFare(passengerBeanLocal2);
		passengerList.add(passengerBeanLocal2);
		
		PassengerBean passengerBeanLocal3 = new PassengerBean();
		passengerBeanLocal3.setStops(4);
		passengerBeanLocal3.setFrequentRider(false);
		passengerBeanLocal3.setRequestedNewspaper(1);
		commuter.caclFare(passengerBeanLocal3);
		passengerList.add(passengerBeanLocal3);
		
		PassengerBean passengerBeanLocal4= new PassengerBean();
		passengerBeanLocal4.setMiles(90);
		passengerBeanLocal4.setRequestedNewspaper(1);
		vacation.caclFare(passengerBeanLocal4);
		passengerList.add(passengerBeanLocal4);
		
		PassengerBean passengerBeanLocal5 = new PassengerBean();
		passengerBeanLocal5.setMiles(199);
		passengerBeanLocal5.setRequestedNewspaper(1);
		vacation.caclFare(passengerBeanLocal5);
		passengerList.add(passengerBeanLocal5);
		
		double actual = journey.totalFare(passengerList);
		assertEquals(expected, actual,.001);
	}
	
	


}
