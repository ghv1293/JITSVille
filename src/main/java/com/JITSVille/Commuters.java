package com.JITSVille;

import java.util.List;

public class Commuters extends Passenger{
	
	double fare;

	@Override
	public double caclFare(PassengerBean pb ) {
		
		fare = getRateFactor()*pb.getStops();
		// TODO Auto-generated method stub
		if(pb.isFrequentRider()) {
			
			fare = fare-fare*0.1;
			
		}
		pb.setCalculatedFare(fare);
		return fare;
	}
}
