package com.JITSVille;

import java.util.ArrayList;
import java.util.List;

public abstract class Passenger {
	
	
	private double rateFactor =0.5;
	
	public double getRateFactor() {
		return rateFactor;
	}
	
/*	List<PassengerBean> allPassengers = new ArrayList<PassengerBean>();

	public List<PassengerBean> getAllPassengers() {
		return allPassengers;
	}*/

	public abstract double caclFare(PassengerBean pb);

}
