package com.JITSVille;

public class PassengerBean {
	private int stops;
	private boolean frequentRider;
	private double calculatedFare;
	private int requestedNewspaper;
	private int calculatedMeals;
	public int getCalculatedMeals() {
		return calculatedMeals;
	}
	public void setCalculatedMeals(int calculatedMeals) {
		this.calculatedMeals = calculatedMeals;
	}
	public double getCalculatedFare() {
		return calculatedFare;
	}
	public void setCalculatedFare(double calculatedFare) {
		this.calculatedFare = calculatedFare;
	}
	public int getRequestedNewspaper() {
		return requestedNewspaper;
	}
	public void setRequestedNewspaper(int requestedNewspaper) {
		this.requestedNewspaper = requestedNewspaper;
	}
	public int getMiles() {
		return miles;
	}
	public void setMiles(int miles) {
		this.miles = miles;
	}
	private int miles;
	public int getStops() {
		return stops;
	}
	public void setStops(int stops) {
		this.stops = stops;
	}
	public boolean isFrequentRider() {
		return frequentRider;
	}
	public void setFrequentRider(boolean frequentRider) {
		this.frequentRider = frequentRider;
	}
	@Override
	public String toString() {
		return "PassengerBean [stops=" + stops + ", frequentRider=" + frequentRider + ", calculatedFare="
				+ calculatedFare + ", requestedNewspaper=" + requestedNewspaper + ", calculatedMeals=" + calculatedMeals
				+ ", miles=" + miles + "]";
	}

}
