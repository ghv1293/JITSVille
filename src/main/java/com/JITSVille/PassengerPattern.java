package com.JITSVille;

public class PassengerPattern {
	
	 public Passenger getPassenger(String passengerType){
	      if(passengerType == null){
	         return null;
	      }		
	      if(passengerType.equalsIgnoreCase("Commuter")){
	         return new Commuters();
	         
	      } else if(passengerType.equalsIgnoreCase("Vacation")){
	         return new Vacation();
	      }
	      
	      return null;
	      
	 }

}
