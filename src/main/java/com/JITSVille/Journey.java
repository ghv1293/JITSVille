package com.JITSVille;

import java.util.List;

public class Journey {
	
	public int totalNewspaper(List<PassengerBean> passengerList) {
		
		int count=0;
		for(PassengerBean passengerBean : passengerList) {
			count+=passengerBean.getRequestedNewspaper();
		}
		return count;
	}
	
	public int totalMeals(List<PassengerBean> passengerList) {
		int count=0;
		for(PassengerBean passengerBean : passengerList) {
			count+=passengerBean.getCalculatedMeals();
		}
		return count;
	}
	
	public double totalFare(List<PassengerBean> passengerList) {
		double count=0;
		for(PassengerBean passengerBean : passengerList) {
			count+=passengerBean.getCalculatedFare();
		}
		return count;
	}

}
